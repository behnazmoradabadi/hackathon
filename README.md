# Prerequisite

### Access to RSC portals

Please verify you can access to these portals with these credentials:

- Application Market:

  - url: [https://application-market-admin.rsc-pilot2.surfresearchcloud.nl/admin](https://application-market-admin.rsc-pilot2.surfresearchcloud.nl/admin)
  - username: `hackathon_user`
  - password: `researchcloud`

- Workspace: 

  - url: [https://workspace-admin.rsc-pilot2.surfresearchcloud.nl/admin](https://workspace-admin.rsc-pilot2.surfresearchcloud.nl/admin)
  - username: `hackathon_user`
  - password: `researchcloud`

- Portal:

  - url: https://portal.rsc-pilot2.surfresearchcloud.nl/

- Reset time based password

  

### Gitlab.com Account

Please create an account in Gitlab.com if you don't have.

### SSH KEY

Please upload your ssh key in SRAM service (In profile section): https://sbs.sram.surf.nl/home

# Create Your First App

### Create plugin

- Login into gitlab.com

- Create a public Repository named: `<your-name-plugin>`

- Create an ansible file (called `<yourname>-plugin.yml`) in your repository that creates a folder and the folder name is a parameter:

  ```
  - name: Sample ansible remote
    hosts: localhost
  
    tasks:
      - name: Create ansible-playbook-directory
        file:
          path: /behnaz/{{ folder_name }}
          state: directory
          recurse: yes
  ```

- Now create a plugin in Application market with these information:

  - Name: `<your-name-plugin>`
  - Script: Refers to the repo you creates and the path is the path of your ansible playbook in the repository.
  - Icon: You can put base64PNG format for Icon.
  - Define the parameter you have in your plugin and put a default value for that.
  - Host: remote

### Create Application

Now create one application with these information:

- Name: `<your-name-application>`
- Icon: You can put base64PNG format for Icon.
- Access format: `ssh://ubuntu@==IP==`
- Plugins:
  - RSC-OS
  - RSC-CO
  - RSC-External-Plugin
  - `<your-name-plugin>`

### Create Application Offering

Now create one application offering with these information:

- Name: `<your-name-app-offering>`
- Application: Your application
- Subscription: `Openstack-HPC SURF`
- Flavour: `Openstack-HPC Surf-Small VM- Ubuntu 18`

### Create a workspace

please create a workspace from the app you created using . Please use Hackathon CO in creating workspace. 

In meanwhile while your workspace is being created please go to workspace portal -> workspace section. Search your workspace. Open it and check the logs. 

After the workspace is being created please ssh into that using

`sram_username@ip`

Check if your folder has been created or not.

Try to run `sudo su` command. Use totp code. You should be able to become root.

### Delete workspace

Now please remove your workspace.

### Override plugin parameter in application:

Now please override the parameter in plugin to another value.

Create a new workspace again and check if overriding works or not.

### Add CO restriction

Restrict the access of your App to just CO: `2232332332`. 

Refresh the portal (it is a bug. Will be fixed in the next sprint). Then please check that you are not able to create workspace with that APP anymore.

### Delete things

Well done. Now please delete these things:

- Your App offering

- Your App

- Your Plugin

- Your repo

  